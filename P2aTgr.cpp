// Program 2a: Roman Order
// Convert decimal input to Roman Numerals
// Authors: Julie Rosen, Kelly Morales

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main(){
  string numerals[] = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
  int nVals[] = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
  vector <string> RN;
  int val;
  while (cin >> val){
    string rn = "";
    for (int i = 0; i < 13; i++){
      while (val >= nVals[i]){
	val -= nVals[i];
	rn += numerals[i];
      }
    }
    RN.push_back(rn);
  }
  sort(RN.begin(), RN.end());
  for (int i = 0; i < RN.size(); i++)
    cout << RN[i] << endl;
}
